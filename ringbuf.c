#include <stdio.h>
#include "ringbuf.h"
#include "tree.h"

void ringbuf_debug (ringbuf_t *buf)
{
	int i;

	if (ringbuf_is_empty (buf))
	{
		printf("the buffer is empty\n");
		return;
	}
	printf("____ buffer debug:\n");
	for (i = (buf->head) % BUFFER_SIZE; i < (buf->tail) % BUFFER_SIZE; i++)
		printf("%d \n", buf->queue[i].data);
	printf("____ end buffer debug:\n");
}

void ringbuf_init (ringbuf_t *buf)
{
	buf->head = 0;
	buf->tail = 0;
}

int ringbuf_is_empty (ringbuf_t *buf)
{
	return buf->head == buf->tail;
}

int ringbuf_is_full (ringbuf_t *buf)
{
	return (buf->tail + 1) % BUFFER_SIZE == buf->head;
}


void ringbuf_enqueue (ringbuf_t *buf, node_t *node, int *is_full)
{
	if (ringbuf_is_full (buf))
    {
        *is_full = 1;
	    return;
    }

    *is_full = 0;

	buf->queue[buf->tail] = *node;
	buf->tail = (buf->tail + 1) % BUFFER_SIZE;
}

node_t *ringbuf_dequeue (ringbuf_t *buf, int *is_empty)
{
	node_t *tmp;
	if (ringbuf_is_empty (buf))
	{
		*is_empty = 1;
		return NULL;
	}

	*is_empty = 0;

	tmp = &(buf->queue[buf->head]);
	buf->head = (buf->head + 1) % BUFFER_SIZE;
	return tmp;
}

