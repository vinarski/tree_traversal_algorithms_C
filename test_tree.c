#include "tree.h"

int main (void)
{
	node_t *root = NULL;
	node_t *value;
	tree_iter_t it;
	void (*callback) (int);
	tree_iter_levelorder_t iter;
	/*
	int i;
	for (i = 1; i<= 9; i+=2)
		root = tree_add_node (root, i);
	for (i = 2; i<= 10; i+=2)
		root = tree_add_node (root, i);
	*/
	//construct a test tree
	root = tree_add_node (root, 6);
	root = tree_add_node (root, 1);
	root = tree_add_node (root, 3);
	root = tree_add_node (root, 10);
	root = tree_add_node (root, 8);
	printf ("-------------\n");
	printf ("tree_search\n");
	printf ("key is 3\n");
	value = tree_search (root, 3);
	if (value == NULL)
		printf ("key not found\n");
	else
		printf ("found: %d \n", value->data);
	printf ("-------------\n");
	printf ("pre_order_rec\n");
	pre_order_rec (root);
	printf ("-------------\n");
	printf ("in_order_rec\n");
	in_order_rec (root);
	printf ("-------------\n");
	printf ("post_order_rec\n");
	post_order_rec (root);
	printf ("-------------\n");
	printf ("in_order_iter\n");
	in_order_iter (root);
	printf ("-------------\n");
	printf ("in_order_iter2\n");
	in_order_iter (root);
	printf ("-------------\n");
	printf ("pre_order_iter\n");
	pre_order_iter (root);
	printf ("-------------\n");
	printf ("tree_count_nodes_rec\n");
	printf ("nodes count: %d\n", tree_count_nodes_rec(root));
	printf ("-------------\n");
	printf ("tree_search\n");
	printf ("key is 3\n");
	value = tree_search (root, 3);
	if (value == NULL)
		printf ("key not found\n");
	else
		printf ("found: %d \n", value->data);
	printf ("tree_search\n");
	printf ("key is 2\n");
	value = tree_search (root,2);
	if (value == NULL)
		printf ("key not found\n");
	else
		printf ("found: %d \n", value->data);
	printf ("-------------\n");
	printf ("level_order_iter\n");
	level_order_iter(root);
	printf ("-------------\n");
	init_tree_iter_inorder (root, &it);
	while (tree_iter_inorder_has_next (&it))
        printf ("%d \n", (tree_iter_inorder_get_next (&it))->data);
    printf ("-------------\n");
    printf ("level_order_iter_visitor\n");
    callback = &some_callback_function;//void (*callback) (int);
    level_order_iter_visitor(root, callback);
    printf ("°°°°°°°°°°°°°°°°\n");
    level_order_iter_visitor (root, &some_callback_function);
    printf ("-------------\n");
    printf ("!!!level_order_iter iterator\n");
    init_tree_iter_levelorder (root, &iter);
    while (tree_iter_levelorder_has_next (&iter))
        printf ("%d\n", tree_iter_levelorder_get_next (&iter)->data);
	return 0;
}
