#ifndef TREE_H_
#define TREE_H_

#include <stdio.h>
#include <malloc.h>
#include "ringbuf.h"
#include "node.h"

#define STACK_SIZE 100

typedef struct job_tag
{
	int isRoot;
	node_t *node;
} job_t;

typedef struct tree_iter_tag
{
	int stack_used;
	job_t stack [STACK_SIZE];

} tree_iter_t;

typedef struct tree_iter_levelorder_tag
{
    ringbuf_t buffer;
    node_t curr_node;
} tree_iter_levelorder_t;

node_t* tree_add_node (node_t *root, int data);
void    pre_order_rec (node_t *root);
void    in_order_rec (node_t *root);
void    post_order_rec (node_t *root);
void    level_order_iter (node_t *root);
void    level_order_iter_visitor (node_t *root, void (*callback) (int));
void    some_callback_function(int num);
int     tree_count_nodes_rec (node_t *root);
void    pre_order_iter (node_t *root);
void    in_order_iter (node_t *root);
node_t* tree_search (node_t *root, int data);

void    init_tree_iter_inorder (node_t *root, tree_iter_t *iter);
int     tree_iter_inorder_has_next (tree_iter_t *iter);
node_t* tree_iter_inorder_get_next (tree_iter_t *iter);

void    init_tree_iter_levelorder (node_t *root, tree_iter_levelorder_t *iter);
int     tree_iter_levelorder_has_next (tree_iter_levelorder_t *iter);
node_t* tree_iter_levelorder_get_next (tree_iter_levelorder_t *iter);

#endif //TREE_H_

