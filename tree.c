#include <stdio.h>
#include <malloc.h>
#include "tree.h"
#include "ringbuf.h"

#define STACK_SIZE 100
node_t* tree_add_node (node_t *root, int data)
{
	//create a new node on the heap
	node_t *newNode = malloc (sizeof (node_t));
	//initialize internals
	//(*newNode).data
	//equivalent syntax:
	newNode->data = data;
	newNode->left = NULL;
	newNode->right = NULL;
	//if tree does not exit
	if (root == NULL)
		return newNode;
	//if tree exists
	node_t *currNode = root;
	while (1)
	{
		if (data == currNode->data)
			return root;
		else if (data < currNode->data)
		{
			if (currNode->left == NULL)
			{
				currNode->left = newNode;
				return root;
			}
			else
			{
				currNode = currNode->left;
				continue;
			}
		}
		else
		{
			if (currNode->right == NULL)
			{
				currNode->right = newNode;
				return root;
			}
			else
			{
				currNode = currNode->right;
				continue;
			}
		}
	}
}

void pre_order_rec (node_t *root)
{
	if (root == NULL)
		return;
	printf ("%d\n", root->data);
	pre_order_rec (root->left);
	pre_order_rec (root->right);
}

void in_order_rec (node_t *root)
{
	if (root == NULL)
		return;
	in_order_rec (root->left);
	printf ("%d\n", root->data);
	in_order_rec (root->right);
}

void post_order_rec (node_t *root)
{
	if (root == NULL)
		return;
	post_order_rec (root->left);
	post_order_rec (root->right);
	printf ("%d\n", root->data);
}

void level_order_iter (node_t *root)
{
	ringbuf_t buffer;
    int is_full;

	if (root == NULL)
		return;

	ringbuf_init (&buffer);
    ringbuf_enqueue (&buffer, root, &is_full);
	while (!ringbuf_is_empty(&buffer))
	{
        int is_empty;
		node_t *curr_node = ringbuf_dequeue(&buffer, &is_empty);
		printf("%d\n", curr_node->data);
		if (curr_node->left != NULL)
			ringbuf_enqueue (&buffer, curr_node->left, &is_empty);
		if (curr_node->right != NULL)
			ringbuf_enqueue (&buffer, curr_node->right, &is_empty);
	}
}

void init_tree_iter_levelorder (node_t *root, tree_iter_levelorder_t *iter)
{
    int is_full;

    if (root == NULL)
        return;

    ringbuf_init (&(iter->buffer));
    ringbuf_enqueue (&(iter->buffer), root, &is_full);
}

int tree_iter_levelorder_has_next (tree_iter_levelorder_t *iter)
{
    return !ringbuf_is_empty (&(iter->buffer));
}

node_t *tree_iter_levelorder_get_next (tree_iter_levelorder_t *iter)
{
    int is_full = 0;
    int is_empty;

    if (tree_iter_levelorder_has_next (iter))
    {
        iter->curr_node = *ringbuf_dequeue (&(iter->buffer), &is_empty);

        if (!is_full && iter->curr_node.left != NULL)
        {
            ringbuf_enqueue (&(iter->buffer), iter->curr_node.left, &is_full);
        }
        if (!is_full && iter->curr_node.right != NULL)
            {
            ringbuf_enqueue (&(iter->buffer), iter->curr_node.right, &is_full);
            if (is_full)
                return &(iter->curr_node);
            }
        return &(iter->curr_node);
    }
    return &(iter->curr_node);
}

void some_callback_function (int num) {printf("%d\n", num);}

void level_order_iter_visitor (node_t *root, void (*callback) (int))
{
    ringbuf_t buffer;
    int is_full;

    if (root == NULL)
        return;

    ringbuf_init (&buffer);
    ringbuf_enqueue (&buffer, root, &is_full);

    while (!ringbuf_is_empty(&buffer))
    {
        int is_empty;
        node_t *curr_node = ringbuf_dequeue (&buffer, &is_empty);

        callback(curr_node->data); // equivalent (*callback)(curr_node.data);

        if (curr_node->left != NULL)
            ringbuf_enqueue (&buffer, curr_node->left, &is_empty);
        if (curr_node->right != NULL)
            ringbuf_enqueue (&buffer, curr_node->right, &is_empty);
    }
}

int tree_count_nodes_rec (node_t *root)
{
	if (root == NULL)
		return 0;
	return 1 + tree_count_nodes_rec (root->left)
             + tree_count_nodes_rec (root->right);
}

void pre_order_iter (node_t *root)
{
	int stack_used = 0;
	job_t stack [STACK_SIZE];

	if (root == NULL)
		return;

	printf ("%d\n", root->data);

	stack[stack_used].node = root->right;
	stack_used++;
	stack[stack_used].node = root->left;
	stack_used++;

	while (stack_used > 0)
	{
		job_t job = stack[stack_used - 1];
		stack_used--;

		if (job.node == NULL)
			continue;

		printf ("%d\n", job.node->data);
		stack[stack_used].node = job.node->right;
		stack_used++;
		stack[stack_used].node = job.node->left;
		stack_used++;
	}
}

void in_order_iter (node_t *root)
{
	//in C you are able to create an array of struct type
	int stack_used = 0;
	job_t stack [STACK_SIZE];
	if (root == NULL)
		return;

	//in-order:  right, root, left
	//put right on the stack
	stack[stack_used].isRoot = 0;
	stack[stack_used].node   = root->right;
	stack_used++;

	//put root on the stack
	stack[stack_used].isRoot = 1;
	stack[stack_used].node   = root;
	stack_used++;

	//put left on the stack
	stack[stack_used].isRoot = 0;
	stack[stack_used].node   = root->left;
	stack_used++;

	while (stack_used != 0)
	{
		job_t job = stack[stack_used - 1];
		stack_used--;

		if (job.node == NULL)
			continue;

		if (job.isRoot)
		{
			printf("%d\n", job.node->data);
			continue;
		}

		//put right on the stack
		stack[stack_used].isRoot = 0;
		stack[stack_used].node   = job.node->right;
		stack_used++;

		//put root on the stack
		stack[stack_used].isRoot = 1;
		stack[stack_used].node   = job.node;
		stack_used++;

		//put left on the stack
		stack[stack_used].isRoot = 0;
		stack[stack_used].node   = job.node->left;
		stack_used++;
	}
}

void init_tree_iter_inorder (node_t *root, tree_iter_t *iter)
{
	if (root == NULL || iter == NULL)
		return;

    iter->stack_used = 0;

	//in-order:  right, root, left
	//put right on the stack
	if (root->right != NULL)
	{
		iter->stack[iter->stack_used].isRoot = 0;
		iter->stack[iter->stack_used].node   = root->right;
		iter->stack_used++;
	}

	//put root on the stack
	iter->stack[iter->stack_used].isRoot = 1;
	iter->stack[iter->stack_used].node   = root;
	iter->stack_used++;

	//put left on the stack
	if (root->left != NULL)
	{
		iter->stack[iter->stack_used].isRoot = 0;
		iter->stack[iter->stack_used].node   = root->left;
		iter->stack_used++;
	}
}

int tree_iter_inorder_has_next (tree_iter_t *iter)
{
	return iter->stack_used != 0;
}

node_t* tree_iter_inorder_get_next (tree_iter_t *iter)
{
	if (!tree_iter_inorder_has_next (iter))
		return NULL;

	while (1)
	{
        job_t curr_job = iter->stack[iter->stack_used - 1];
        iter->stack_used --;

        if (curr_job.isRoot)
            return curr_job.node;

		if (curr_job.node->right != NULL)
		{
			iter->stack[iter->stack_used].isRoot = 0;
			iter->stack[iter->stack_used].node = curr_job.node->right;
			iter->stack_used++;
		}

		iter->stack[iter->stack_used].isRoot = 1;
		iter->stack[iter->stack_used].node = curr_job.node;
		iter->stack_used++;

		if (curr_job.node->left != NULL)
		{
			iter->stack[iter->stack_used].isRoot = 0;
			iter->stack[iter->stack_used].node = curr_job.node->left;
			iter->stack_used++;
		}
	}
}

void in_order_iter2 (node_t *root)
{
	//in C you are able to create an array of struct type
	int stack_used = 0;
	job_t stack [STACK_SIZE];

	if (root == NULL)
		return;

	//in-order:  right, root, left
	//put right on the stack
	if (root->right != NULL)
	{
		stack[stack_used].isRoot = 0;
		stack[stack_used].node   = root->right;
		stack_used++;
	}
	//put root on the stack
	stack[stack_used].isRoot = 1;
	stack[stack_used].node   = root;
	stack_used++;
	//put left on the stack
	if (root->left != NULL)
	{
		stack[stack_used].isRoot = 0;
		stack[stack_used].node   = root->left;
		stack_used++;
	}
	while (stack_used != 0)
	{
		job_t job = stack[stack_used - 1];
		stack_used--;
		if (job.isRoot)
		{
			printf("%d\n", job.node->data);
			continue;
		}
		//put right on the stack
		if (job.node->right != NULL)
		{
			stack[stack_used].isRoot = 0;
			stack[stack_used].node   = job.node->right;
			stack_used++;
		}
		//put root on the stack
		stack[stack_used].isRoot = 1;
		stack[stack_used].node   = job.node;
		stack_used++;
		//put left on the stack
		if (job.node->left != NULL)
		{
			stack[stack_used].isRoot = 0;
			stack[stack_used].node   = job.node->left;
			stack_used++;
		}
	}
}

node_t* tree_search (node_t *root, int data) //data is the key
{
	if (root == NULL)
		return NULL;
	if (data == root->data)
		return root;
	if (data < root->data)
		return tree_search (root->left, data);
	else
		return tree_search (root->right, data);
}

