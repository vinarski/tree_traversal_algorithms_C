#ifndef NODE_H_
#define NODE_H_

typedef struct node_tag
{
    int data;
    // node_t *left; -> does not compile since the alias is defined below and so is not yet known here
    struct node_tag *left; //the keyword struct is needed if you use the original name while declaring type
    struct node_tag *right;
} node_t;

#endif /* NODE_H_ */
