#ifndef RINGBUF_H_
#define RINGBUF_H_

#include "node.h"

#define BUFFER_SIZE 101

typedef struct ringbuf_tag
{
	node_t queue[BUFFER_SIZE];
	unsigned short head;
	unsigned short tail;
}ringbuf_t;

void    ringbuf_debug (ringbuf_t *buf);
void    ringbuf_init (ringbuf_t *buf);
int     ringbuf_is_empty (ringbuf_t *buf);
int     ringbuf_is_full (ringbuf_t *buf);
void    ringbuf_enqueue (ringbuf_t *buf, node_t *node, int *is_full);
node_t* ringbuf_dequeue (ringbuf_t *buf, int *is_empty);

#endif

